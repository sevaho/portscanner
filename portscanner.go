// -----------------------------------------------------------------------------------------------------------------------------
// GENERAL
// -----------------------------------------------------------------------------------------------------------------------------
//
// author: Sebastiaan Van Hoecke
// mail: sebastiaan@sevaho.io
//
// NOTE:
//
// -----------------------------------------------------------------------------------------------------------------------------

package main

import (

    "fmt"
    "strconv"
    "net"
    "time"

)

// -----------------------------------------------------------------------------------------------------------------------------
// VARIABLES
// -----------------------------------------------------------------------------------------------------------------------------

type port struct {

    open    bool
    number  int

}

var m = make(map[int]bool)

// -----------------------------------------------------------------------------------------------------------------------------
// FUNCTIONS
// -----------------------------------------------------------------------------------------------------------------------------

func check (err error) {

    if err != nil {


    }

}

func checkPort (host string, portnr int, ch chan<- port) {

    t   := time.Millisecond * 5000
    b   := false

    bs  := make([]byte, 30)
    bl  := 0
    bl  += copy(bs[bl:], host)
    bl  += copy(bs[bl:], ":")
    bl  += copy(bs[bl:], strconv.Itoa(portnr))

    // f, e := net.DialTimeout("tcp", string(bs), t)

    tcpAddr, err := net.ResolveTCPAddr("tcp4", string(bs))

	if err != nil {

        b = false

	}

	_, err = net.DialTimeout("tcp", tcpAddr.String(), t)

	if err != nil {

        b = false

	} else {

        b = true

    }

    ch<- port{b, portnr}

}

// -----------------------------------------------------------------------------------------------------------------------------
// MAIN
// -----------------------------------------------------------------------------------------------------------------------------

func main () {

    ch  := make(chan port)
    ip  := "78.21.146.62"

    for i := 0; i < 1000; i++ {

        go checkPort(ip, i, ch)

    }

    for i := 0; i < 1000; i++ {

        fmt.Println(<-ch)

    }

    // ip  := "78.21.146.62"
    //
    // t   := time.Millisecond * 2000
    //
    // for i := 0; i < 444; i++ {
    //
    //     b   := false
    //
    //     bs  := make([]byte, 30)
    //     bl  := 0
    //     bl  += copy(bs[bl:], ip)
    //     bl  += copy(bs[bl:], ":")
    //     bl  += copy(bs[bl:], strconv.Itoa(i))
    //
    //     go func (ee int) {
    //
    //         tcpAddr, err := net.ResolveTCPAddr("tcp4", string(bs))
    //
    //         if err != nil {
    //
    //             b = false
    //
    //         }
    //
    //         conn, err := net.DialTimeout("tcp", tcpAddr.String(), t)
    //
    //         if err != nil {
    //
    //             b = false
    //
    //         } else {
    //
    //             b = true
    //             fmt.Println(conn, string(bs))
    //
    //         }
    //
    //     }(i)
    //
    // }
    //
    // time.Sleep(5 * time.Second)

}
